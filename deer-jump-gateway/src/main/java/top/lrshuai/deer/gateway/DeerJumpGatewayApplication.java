package top.lrshuai.deer.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeerJumpGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeerJumpGatewayApplication.class, args);
	}

}
