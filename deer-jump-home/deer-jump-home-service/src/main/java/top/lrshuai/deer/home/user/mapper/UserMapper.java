package top.lrshuai.deer.home.user.mapper;

import top.lrshuai.deer.home.user.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author rstyro
 * @since 2021-07-27
 */
public interface UserMapper extends BaseMapper<User> {

}
