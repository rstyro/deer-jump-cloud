package top.lrshuai.deer.home.user.service;

import top.lrshuai.deer.home.user.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author rstyro
 * @since 2021-07-27
 */
public interface IUserService extends IService<User> {

}
