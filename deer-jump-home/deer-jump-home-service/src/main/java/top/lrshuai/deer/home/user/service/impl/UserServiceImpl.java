package top.lrshuai.deer.home.user.service.impl;

import top.lrshuai.deer.home.user.entity.User;
import top.lrshuai.deer.home.user.mapper.UserMapper;
import top.lrshuai.deer.home.user.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author rstyro
 * @since 2021-07-27
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
