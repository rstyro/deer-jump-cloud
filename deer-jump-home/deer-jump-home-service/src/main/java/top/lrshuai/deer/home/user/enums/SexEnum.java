package top.lrshuai.deer.home.user.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

@Getter
public enum SexEnum {

    UNKNOWN(0,"保密"),
    BOY(1,"男"),
    GIRL(2,"女"),
    ;


    @EnumValue
    private final Integer value;

    @JsonValue
    private String desc;

    SexEnum(int value,String desc){
        this.value=value;
        this.desc=desc;

    }
}
