package top.lrshuai.deer.home;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScans(value = {
		@MapperScan(value = "top.lrshuai.deer.home.*.mapper*")
})
@SpringBootApplication
public class DeerJumpHomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeerJumpHomeApplication.class, args);
	}

}
