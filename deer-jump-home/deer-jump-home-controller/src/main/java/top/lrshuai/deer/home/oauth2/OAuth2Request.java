package top.lrshuai.deer.home.oauth2;

public interface OAuth2Request {

    public String getOpenId();
    public OAuth2Token getAccessToken();
    public OAuth2User getUserInfo();
}
