package top.lrshuai.deer.home.oauth2.impl;

import cn.hutool.core.net.url.UrlBuilder;
import cn.hutool.http.HttpUtil;
import top.lrshuai.deer.home.oauth2.OAuth2Config;
import top.lrshuai.deer.home.oauth2.OAuth2Request;
import top.lrshuai.deer.home.oauth2.OAuth2Token;
import top.lrshuai.deer.home.oauth2.OAuth2User;

public class OAuth2QqRequest implements OAuth2Request {

    private OAuth2Config oAuth2Config;

    @Override
    public String getOpenId() {
        return null;
    }

    @Override
    public OAuth2Token getAccessToken() {
        return null;
    }

    @Override
    public OAuth2User getUserInfo() {
        return null;
    }

    public String getAccessTokenUrl(){
        HttpUtil.createGet(oAuth2Config.getRedirectUri());
        return null;
    }
}
