package top.lrshuai.deer.home.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.lrshuai.deer.home.user.service.IUserService;
import top.lrshuai.nacos.commons.R;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author rstyro
 * @since 2021-07-27
 */
@RestController
@RequestMapping("/user/user")
public class UserController {

    private IUserService userService;

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping("/test")
    public R test(){
        return R.ok();
    }

    @GetMapping("/list")
    public R list(){
        return R.ok(userService.list());
    }
}
