package top.lrshuai.deer.home.oauth2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class OAuth2Config {
    /**
     * 客户端id：对应各平台的appKey
     */
    private String clientId;

    /**
     * 客户端Secret：对应各平台的appSecret
     */
    private String clientSecret;

    /**
     * 登录成功后的回调地址
     */
    private String redirectUri;

    /**
     * QQ可选
     * 是否需要申请unionid，平台唯一性，同一用户，对同一个QQ互联平台下的不同应用，unionID是相同的。
     */
    private Boolean unionId;

}
